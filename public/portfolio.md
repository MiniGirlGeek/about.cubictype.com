[brutal]: #title "CubicType portfolio"

<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- from fonts.google.com selector -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Bitter&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Mono&display=swap" rel="stylesheet">

<style>
body {
  font-family: 'Bitter', serif;
  margin: 8px;
}

h1 {
  font-family: 'League Gothic', sans-serif;
  font-size: 3em;
  font-weight: normal;
  margin-bottom: 2px;
  margin-block-start: 0;
}

h2 {
  font-size: 2em;
  font-weight: bold;
  margin-block-end: 0.5em;
  margin-block-start: 0.5em;
  margin-left: -8px;
  margin-right: -8px;
  padding: 0 8px;
  background-color: #222;
  color: #fff
}

tt, code, kbd, samp {
    font-family: 'IBM Plex Mono';
}

pre, xmp, plaintext, listing {
    font-family: 'IBM Plex Mono';
}

/* reduce the indent */
ul {
  padding-left: 1em;
}

figure {
    margin-left: 1.5em;
    margin-right: 1.5em;
}

figure.diptych {
    max-width: 48%;
    display: inline-block;
    margin-right: 0px;
    margin-left: 0px;
}

figcaption {
  font-family: 'PT Sans Caption', sans-serif;
  font-size: 0.875em;
}

figure.diptych figcaption {
  padding-left: 0.5em;
  padding-right: 0.5em;
}

/* latin-ext */
@font-face {
  font-family: 'League Gothic';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(static/WOFF/LeagueGothic-Regular.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'League Gothic';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(static/WOFF/LeagueGothic-Regular.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

/* latin */
@font-face {
  font-family: 'PT Sans Caption';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(static/TTF/PTSansCaption-Regular.ttf) format('opentype');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

</style>

This is the portfolio of aspirant type designer David Jones.

[Reach me on Twitter](https://twitter.com/drjtwit/).

## Atwin $ a font

[Atwin has been available commercially since August
2021](https://www.myfonts.com/fonts/cubic-type/atwin/).

<img alt="Sample alphabet in Atwin" src="at-sample.svg"
title="Atwin" style="max-width: 100vw; max-height: 100vh;" width="100%">

Working from an iPhone photo of a page from the Phil's Photo specimen
catalogue «Homage to the Alphabet» that [@stewf posted to the flickr
feed](https://www.flickr.com/photos/stewf/16437312062/in/album-72157636319098466/),
i traced this sample by hand into a basic font, using Glyphs Mini.

The numbers in this sample are identical (as far as this reproduction
allows) to the MICR numbers used in bank cheque processing.
The letters are an extraordinary extension.
Most of the soft chamfers are round, based on circles or ellipses.
At first glance it may seem that the same shapes have been duplicated,
but in fact many of the strokes and curves have been subtlety modified:
The counters of E do not have the same curve at the top as at the bottom.

<img alt="Outline of the E, showing one inside curve is 33 by 33, the other
is 33 by 61" src="at-E.svg"
title="Counters, differing curves"
style="max-width: 100vw; max-height: 100vh;" width="100%">

My version has been spaced and kerned fairly tightly
to follow the spirit of the sample.
The original sample had no diacritic marks,
my current version of Atwin supports: dieresis, dot (above),
grave, acute, circumflex, caron, breve, ring, tilde, macron,
cedilla.
All these have been drawn afresh.

The original sample has very few punctuation marks, i have drawn a
more complete set of punctuation.
Some of these are trivial (like `questionreversed`, which is a
reflection of **?**), others are new drawings.
Parentheses, braces, slashes, typographic quotes, and
even glyphs like `tilderingabove` have been drawn.

Stems and metrics have been adjusted to be consistent and ready
for modern rendering and hinting engines.


## Avimode $ a new font

[Avimode has been available commercially since April 2022](https://www.myfonts.com/fonts/cubic-type/avimode/).

<img alt="The alphabet in a random order, drawn in light blue on
black, using the font Avimod" src="avimode-plaque.svg"
title="Avimode, alphabet"
style="max-width: 100vw; max-height: 100vh;" width="100%">

_Avimode_ is an original design from CubicType.
A preliminary version is available commercially, and
we expect to continue to expand it.

Inspired by a fusion of ideas:

- counters should have counters inside them;
- circuit-board traces and _vias_;
- Fill The Space.

Avimode had a couple of letters sketched very roughly using
pencil-on-paper, then the alphabet,
without punctuation or accents, was created digitally over a
couple of days.

Avimode has numbers, punctuation, and reasonably good accent
support.


## Airvent, a font

<figure>
<img alt="The regular upper case alphabet in a random order"
src="airvent-upper-plaque.svg"
style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>
Airvent upper case
</figcaption>
</figure>

_Airvent_ is a trace of
an extremely poor scan of USA military standard 33558.
The lettering was originally designed for panels and
instrumentation for the air force.

The design is a utility monoline sans-serif with rounded terminals.
I have made the font fairly regular, despite the roughness and
irregularity of the specimen.
I have made angled corners even where
they might not have been angled in the original design (in **D**
and **E** and so on).
The **O** and **Q** in the original sample seem heavier than
the other letters, i have regularised their weights, but
also kept more authentic outlines in the design file.

The design is simple, which lends itself to further
manipulations, extensions, and treatments.

I have added a few more latin letters and
a small amount of punctuation and symbols.
Diacritic/accent support is limited: **A** **O** **U** can have
umlauts. As an experiment, i have drawn them in compact style
keeping the marks within the cap-height so they can be used in
titling.

I have added a set of smallcaps letters, smallcaps numbers, and
a smaller set of numbers used for fractions, superior numbers
(footnotes), and (scientific) inferior numbers.


## Ranalina, a font

<img alt="An alphabet in the font Ranalina" src="r-alpha.svg" title="a Ranalina alphabet" style="max-width: 100vmin;" width="100%">

The origin of Ranalina involves:

- found materials in protest signs of the Black Lives Matter protests, and
  other protests of the early 21st century;
- found tools, repurposed from an 8-bit font project, that
  could make font files from straight-line vectors;
- a found 8-bit aesthetic and making curves with a square grid of pixels.

In the 1980s, computer fonts were made on an 8x8 pixel grid.

<figure>
<img alt="The letter B drawn using the ZX Spectrum ROM font, light green on
a dark green grid" src="B.svg" title="B" style="max-width: 80vmin" width="100%">
<figcaption>
“B”, Sinclair ROM font from the ZX Spectrum, 1982.
</figcaption>
</figure>

There are no curves, no diagonals.
2 rectangles touch at a corner.
That is enough to let the eye create the curve in the mind.
An idea that is explored in an essay in Toshi Omagari's «Arcade Game
Typography» (Thames and Hudson, 2019).

I imagined i could create letters using tape,
and instead of curves,
i would bring rectangles together at their corners.

Question: Can i make a font from tape?

<img alt="A question mark on a grid" src="r-query.svg" title="question"
style="max-width: 100vw; max-height: 100vh;" width="100%">

The name _Ranalina_ comes from
[_Rana_, the scientific name for the genus of frogs](https://en.wikipedia.org/wiki/Rana_(genus)), and
Frogtape, a brand of masking tape.

The early experiments were drawing B and O.
I had to attack the curved forms first, to explore the design space, and
test the possibilities.

An early O had the outline of a bevelled rectangle,
giving it a look rather like ITC Machine (ITC, 1970) or
Princetown (Letraset, 1981).

<figure>
<img alt="O drawn in ITC Machine" src="m-O.svg" title="“O”, ITC Machine"
style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>
“O”, ITC Machine (ITC, 1970)
</figcaption>
</figure>

This proved possible, but i was overwhelmed by the design possibilities.
If i am allowed to cut the tape at a 45° angle, what about any angle?
With enough slivers of tape cut at any angle, we could make any shape.
What then, did it mean to make a font from tape?

[image of O with bevelled corners]

[image of made up letter with thin slivers]

I eliminated the possibilities by
declaring that i would only cut the tape at right angles.

The early experiments with the B and O led to
early fixing of some design parameters that have formed
the basis of the remaining font design.
The only polygon in the font's design is a rectangle, and
all the rectangles are as-if made from tape, so
they all share one of their dimensions: the width of the tape,
which is the design's _unit_.
In a scalable font, the design unit can be any size.
However, Ranalina is designed in Inkscape, and
in the SVG source files the design unit is 24mm which matches the width of Frogtape
(in one of its common sizes).

These considerations are a mixture of pragmatic, aesthetic,
and some that constrain the space in order to promote creativity.
Out of these considerations arise The Rules:

- a unit square that is 24mm on a side, with 8 regular subdivisions of 3mm;
- a design grid of 4 units wide and 5 units high;
- rectangles mostly placed vertically or horizontally;
- curves reimagined as rectangles partially overlapping at corners;

The diagonal strokes are more arbitrary than the vertical and horizontal
strokes, but not completely arbitrary:
they are typically placed so that 2 of their corners are
on the 24mm or the 3mm grid.

[illustration of V and grid?]

Of course there are always exceptions to the rules.
And in this font, X is an exception.

<figure>
<img alt="X"
src="X.svg"
title="X" width="100%">
<figcaption>
Exceptional X, in Ranalina
</figcaption>
</figure>

### Tooling

Ranalina is also an exercise and testbed for
the frankly idiosyncratic tools homegrown at CubicType.
It is designed in native SVG and converted to a binary font-file
using software tools entirely created by CubicType.

In this way Ranalina represents a possibility:
making a font free of the proprietary toolchains from Adobe and
other suppliers.

Technically, Ranalina has GPOS features for kerning and mark
positioning (features `kern` and `mark`).

### Influences

A monoline style with a strong grid has a few historical models:

<figure>
<img alt="The text «FORMULATED BY DE STIJL» set in The Foundry Types
Architype Van Doesburg, red on white"
src="avd-thefoundrytypes-sample.svg"
title="van Doesburg sample" width="100%">
<figcaption>
A sample of Architype Van Doesburg,
reproduced from the Architype Konstruct Specimen catalogue from The Foundry Types.
</figcaption>
</figure>

Architype Van Doesburg is
a digital version of Theo van Doesburg's experimental alphabet (1919),
which was originally made from metal type furniture.

Wim Crouwel's New Alphabet (1967), has also been digitised by The Foundry Types:

<figure>
<img alt="The text «New Alphabet created as a four weight family»
in the difficult to decipher New Alphabet, red on white"
src="neu-sample.svg"
title="New Alphabet sample" width="100%">
<figcaption>
A sample
reproduced from the New Alphabet Specimen catalogue from The Foundry Types.
</figcaption>
</figure>

Another example, this time using actual tape to reverse out or mask, is
Ed Ruscha's Boy Scout Utility Modern.
This is not a published font, but has been in use since 1980.

There is certainly nothing original in Theo van Doesburg's idea of
abstracting form into rectangles, and
so this is an idea that recurs throughout history.
Ranalina, which is one expression of that idea,
will inevitably wanly reflect some of those echoes.

## Ranalina, the diacritics

Perhaps unusually for a latin-script font,
Ranalina uses an OpenType `mark` feature to position marks on base glyphs.
This allows for both ordinary use of diacritic marks,
and typographical possibilities beyond the regular unicode
blocks for latin script.

Some possibilities are probably not used by any script yet,
and some are typographically inadvisable.

<figure>
<img alt="The letters ÑŴÜ, followed by Thorn with dot above,
V with ring above, and greater than with macron above"
src="r-diacritic.svg"
title="Some diacritic marks" style="max-width: 100vw; max-height: 100vh" width="100%">
<figcaption>
top line: diacritic marks from existing European languages;
<br>
bottom line: unusual diacritics.
</figcaption>
</figure>

Diacritics are supported for numbers, using the same OpenType `mark` table.
This is useful for the dot-above because mathematicians use that notation
to represent a decimal fraction with a recurring digit.
But it also allows for extra-linguistic possibilities.

<figure>
<img alt="1, 2 with acute above, comma, 3, 4, 5 with dot above / 6, 7 with
grave above, 8, 9, period, 0" src="r-number.svg"
title="numbers and diacritic marks" style="max-width: 100vmin" width="100%">
<figcaption>
top line: 25.4/72 as a decimal fraction;
<br>
bottom line: nonsense diacritic marks on numbers.
</figcaption>
</figure>

Just as letters can be arranged in any combination,
regardless of whether they are words with meaning or not,
diacritic marks and letters should be allowed in any combination.


## Taypography

In the summer of 2020,
i did a Twitter thread
[pairing fonts with Taylor Swift outfits](https://twitter.com/drjtwit/status/1294288363894308870).
This lies within a sort of genre of Twitter threads where
you pair a famous person in various outfits with a collection of other things.

Here's a couple of highlights from that thread.

<figure class="diptych">
<img alt="Taylor Swift in a psychedelic jacket. And extremely long black boots."
src="static/image/swift-psychedelia.png"
title="Swift Psychedelically" style="max-width: 100vw; max-height: 100vh;"
width="100%">
<figcaption>
Taylor Swift at the 2019 MTV Video Music Awards.
</figcaption>
</figure>

<figure class="diptych">
<img alt="Psyche-delia Swift, yellow on a pink field" src="psych-swift.svg"
title="Psychedelia Swift" style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>
Victor Moscoso (K-Type, 2008).
</figcaption>
</figure>

<br>

<figure class="diptych">
<img alt="Taylor Swift in a black dress, showing some leg."
src="static/image/swift-vanity.jpg"
title="Swift Stylishly" style="max-width: 100vw; max-height: 100vh;"
width="100%">
<figcaption>
Taylor Swift at Vanity Fair's after-party for the 2016 Oscars.
</figcaption>
</figure>

<figure class="diptych">
<img alt="The single letter Y" src="honda.svg"
title="Y" style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>
“Y”, ITC Honda (ITC, 1970).
</figcaption>
</figure>


## Auncial, an alphabet

<img alt="An alphabet, including thorn" src="au.svg"
title="Auncial" style="max-width: 100vw; max-height: 100vh;" width="100%">

Auncial is a drawing of an uncial alphabet.
It is modelled, by hand and mouse, from various images found on the internet.
It is my first drawing using an off-the-shelf font creation system (Glyphs Mini).

As early practice, this was useful material in which
i learnt to handle cubic bézier curves.
I hope to revisit this when i have a stronger skill with cubic curves and
a deeper aesthetic of uncial lettering.


## Amplette

_Amplette_ was created using the lettering template that came with
the Poem Edition pamphlet _The material discovery of the alphabet_
by Éloïsa Pérez.
_Amplette_ is an anagram of _template_.

Upper case, originally drawn on paper using the template:

<figure>
<img alt="The regular upper case alphabet in a random order"
src="amplette-regular-upper-plaque.svg"
style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>
Amplette Regular upper case
</figcaption>
</figure>

Lower case, drawn digitally using the same components:

<figure>
<img alt="The regular lower case alphabet in a random order"
src="amplette-regular-lower-plaque.svg"
style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>
Amplette Regular lower case
</figcaption>
</figure>

A more off-piste extension; an uncial alphabet:

<figure>
<img alt="The uncial alphabet in a random order"
src="amplette-uncial-lower-plaque.svg"
style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>
Amplette Uncial
</figcaption>
</figure>


## Akern

<img alt="The upper-case alphabet in a random order, using the font Akern"
  src="akern-uc-plaque.svg"
  title="Akern, alphabet; upsetting kerning"
  style="max-width: 100vw; max-height: 100vh;" width="100%">

<img alt="The lower-case alphabet in a random order, using the font Akern"
  src="akern-lc-plaque.svg"
  title="Akern, alphabet"
  style="max-width: 100vw; max-height: 100vh;" width="100%">

_Akern_
is designed to be fairly mainstream in its execution.
It is an ordinary font that ordinary people can use in ordinary times.

It is currently in a prototype phase.
It has most of the base letters of the Latin script, but
limited diacritics (only breve).
A set of lower-case numbers has been drawn, and
a set of punctuation (limited, but useful).

Some glyphs require minor redrawing, some more extensive.
The spacing and fit has barely begun.

The name is an anagram of Karen,
as in Karen Cheng author of _designing type_.


## artzoid, a font

<img alt="A lower-case alphabet in random order, using the font artzoid, which looks a bit like Ad Lib"
  src="ablang-lower-plaque.svg"
  title="Ablang, alphabet"
  style="max-width: 100vw; max-height: 100vh;" width="100%">

_artzoid_ is a first-draft digitisation of a pencil sketch made in 2021-08.
It was originally targetted for cutting in lino then printing,
and i may well do a digitisation using that physical process
later.

It has only lower case letters (including thorn and eth),
the most popular accents used in English,
minimal punctuation and some extremely draft ¾-height numbers.
The original sketch has some ideas for capitals, but they are
not digitised yet.


## Antscap, an OpenType feature font

<img alt="letters collide touch and join, tape-like thread runs through several letters, an alphabet of disordered sorts"
  src="antscap-upper-plaque.svg"
  title="Antscap, alphabet"
  style="max-width: 100vw; max-height: 100vh;" width="100%">

An experimental font in which the letters are encouraged to
touch and which uses OpenType feature rules to modify the shape
and spacing of letters to enhance the impression that they are
made of a magnetic tape that is slightly chaotic.

Inspired by magnetic tape running over and around capstans.


## Andily, a redraw

<img alt="The upper-case alphabet in a random order, using the font Andily, which looks like Lydian"
  src="andily-upper-plaque.svg"
  title="Andily, alphabet; kerning required"
  style="max-width: 100vw; max-height: 100vh;" width="100%">

<img alt="The lower-case alphabet in a random order, using the font Andily, which looks like Lydian"
  src="andily-lower-plaque.svg"
  title="Andily, alphabet"
  style="max-width: 100vw; max-height: 100vh;" width="100%">

A tracing and redrawing of the font _Lydian_,
working from the USPTO sample.

This is a work-in-progress in its early stages,
the rough digitisation is complete.
Future work will be to redraw the glyphs to consistent metrics,
and consider what new glyphs to draw.
Should it become a fully-fledged font, or a mere alphabet?


## Agon, an obscurity

<figure>    
<img alt="A series of cryptic hexagons, some of which are textured with line-shading; it is in fact an alphabet, but the correspondence to ordinary letters is obscure"
  src="agon-plaque.svg"
  style="max-width: 100vw; max-height: 100vh;" width="100%">
<figcaption>    
Agon alphabet: ABCD / EFGH / IJKLMN / OPQRST / UVWXYZ    
</figcaption>    
</figure>

_Agon_ is an exercise in obscurity and anti-comprehension.
Designed to appeal to the visual processing system of
hypothetical aliens, it eschews curves, strokes, and counters,
in favour of angled and textured hexagons.

I've written a little bit more analysis of Agon on
<a href="https://gitlab.com/drj11/font-agon">the GitLab page for Agon</a>.


## Airbed

<img alt="The letters D O M looking like they have been made from an inflatable material"
  src="airbed-plaque.svg"
  title="Airbed, a few letters"
  style="max-width: 100vw; max-height: 100vh;" width="100%">

An alphabet that has been sketched using pencil-on-paper, but
which has only 3 letters digitised.


## Colophon

Fonts used:

- Title: League Gothic from [The League of Movable Type](https://www.theleagueofmoveabletype.com/);
- Body and section headings: Bitter (Regular, Bold, Italic) from [Huerta Tipográfica](https://huertatipografica.com/);
- Image captions: PT Sans Caption from [ParaType](https://www.paratype.com/);
- Monospace: IBM Plex Mono from IBM.

# END
