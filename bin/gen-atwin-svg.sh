#!/bin/sh

# The Atwin sample is modified in Inkscape after generation.

set -e

hb-view --output-file=at-sample.svg static/OTF/Atwin-Regular.otf --foreground ddddee --background 333333 'GEMINIABCČD
EFGHIJKLMNÑ
OPQRSTU
VWŴXYZ&
[".,:;'\'']?!-$1234
567890¢'
