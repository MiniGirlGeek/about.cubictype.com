#!/bin/sh

set -e

Foreground=115511
Background=ffffdd

hb-view --line-space=-60 --foreground $Foreground --background $Background --output-file=r-alpha.svg static/TTF/ranalina.ttf 'aæbcdðe
fghijklm
nopqrstu
vwxyz&þ'

hb-view --foreground $Foreground --background $Background --output-file=r-number.svg static/TTF/ranalina.ttf '0.3527̇
1́9̂,4̈6̌8̀'

hb-view --foreground $Foreground --background $Background --output-file=r-diacritic.svg static/TTF/ranalina.ttf 'ÑŴÜ
Þ̇V̊>̄'
