# Typography

This is mostly: what fonts?

## Analysis

The text part of a portfolio is like a blog:

- good body text viewable on computers and mobile;
- bold weight for emphasis;
- italic for stress;
- other weights / styles only a nice to have;
- companion mono/tty _with good contrast_;
- good kerning;
- ligatures for clarity, and certainly should not destroy it;
- small caps?
- lower case numbers?
- Welsh support (as a proxy for good latin diacritics)

In addition it will be useful to have another font for titles or display
and one for captions.

So, as a minimum:

- 3 or 4 style body
- title
- 2 style caption

Word of warning: On a Mac, w followed by combining circumflex (Shift-Alt-6)
doesn't produce the true wcircum character, and fonts therefore don't
render it correctly.

Here are the true Wcircum and wcircum: Ŵ ŵ.

## Trial 1

Bitter for body, League Gothic for headlines, PT Sans Caption for captions.
Word of warning here, League Gothic only has (Italic)? (Condensed)?, but
I think that used large in headlines, I won't need a bold.

this is the current get-up and it looks pretty good so far.

I guess I may have to expand the mark tables on PT Sans Caption if i want
to use Welsh.


## Candidate selection


### A bunch of okay but uninteresting fonts

Prompt, Andika New Basic, Source, Lora

### Some good fonts that lack ŵ

PT Sans, Lora

### Some nice fonts

- Vollkorn https://fonts.google.com/specimen/Vollkorn?preview.text_type=custom&preview.text=%C3%B1w%CC%82i%CC%82
- Bitter (from Huerta Tipográfica)  https://fonts.google.com/specimen/Bitter?preview.text_type=custom#standard-styles
- League Mono (for a mono) seems pretty good.


### Some nice fonts lacking the right styles

### Junction

https://www.theleagueofmoveabletype.com/junction

Humanist sans.

I love this it, but. There is no italic.

### League Spartan  https://www.theleagueofmoveabletype.com/league-spartan

Really good, but no italic

### Linden Hill  https://www.theleagueofmoveabletype.com/linden-hill

(deepdene knockoff) Good roman. italic very narrow. no bold.
